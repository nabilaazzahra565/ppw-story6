from django.shortcuts import render, redirect
from .models import Member,Activity
from .forms import MemberForm,ActivityForm
from django.http import HttpResponseRedirect
# Create your views here.

def add_activity(request):
    activity_add = ActivityForm(request.POST or None)

    if request.method == "POST":
        if activity_add.is_valid():
            activity_add.save()

            return HttpResponseRedirect('/')

    context = {
        'ActivityForm' : activity_add,
        'page_title' : 'Add Activity'
        
    }
    return render(request, "add_activity.html", context)



def add_member(request, member_id):
    if request.method =="POST":
        form = MemberForm(request.POST)
        if form.is_valid():
            data = Member()
            data.name = form.cleaned_data['name']
            data.activity = Activity.objects.get(id=member_id)
            data.save()
            return HttpResponseRedirect('/')
        return render(request, 'add_member.html', {'form' : form})
    form = MemberForm()
    return render(request, 'add_member.html', {'form' : form})


def activity_list(request):
    activity = Activity.objects.all()
    member = Member.objects.all()

    context = {
        'activity' : activity,
        'member' : member
    }
    return render(request, "activity_list.html", context)


