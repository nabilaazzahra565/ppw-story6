from django import forms
from .models import Activity,Member

class ActivityForm(forms.ModelForm):
    class Meta:
        model = Activity
        fields = [
            'activity'
        ]

        labels = {
            'activity' : 'Activity',
            
        }

        widgets = {
            'activity': forms.TextInput(
                attrs = {
                    'class' :'form-control',      
                }
            )
        }

class MemberForm(forms.ModelForm):
    class Meta:
        model = Member
        fields = [
            'name'
        ]

        labels = {
            'name' : 'Name',
            
        }

        widgets = {
            'name': forms.TextInput(
                attrs = {
                    'class' :'form-control',      
                }
            )
        }