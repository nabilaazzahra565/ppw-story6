from django.contrib import admin
from django.urls import include, path
from activity import views

urlpatterns = [
    path('', views.activity_list, name="activity_list"),
    path('add-activity/', views.add_activity, name="add_activity"),
    path('add-member/<int:member_id>/', views.add_member, name="add_member"),
]