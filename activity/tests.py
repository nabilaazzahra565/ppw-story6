from django.test import TestCase
from django.urls import resolve, reverse
from django.test import Client
from .models import Activity, Member
from django.apps import apps
from .views import add_activity, add_member, activity_list
from .forms import ActivityForm, MemberForm
from .apps import ActivityConfig

# Create your tests here.
class UrlsTest(TestCase):
    def setUp(self):
        self.activity = Activity.objects.create(
            activity="belajar")
        self.member = Member.objects.create(
            name="Putri", activity=Activity.objects.get(activity="belajar"))
        self.activity_list = reverse("activity_list")
        self.add_activity = reverse("add_activity")
        self.add_member = reverse("add_member", args=[self.activity.pk])

    def test_list_activity_use_right_function(self):
        found = resolve(self.activity_list)
        self.assertEqual(found.func, activity_list)

    def test_add_activity_use_right_function(self):
        found = resolve(self.add_activity)
        self.assertEqual(found.func, add_activity)

    def test_member_use_right_function(self):
        found = resolve(self.add_member)
        self.assertEqual(found.func, add_member)


class ModelTest(TestCase):
    def setUp(self):
        self.activity = Activity.objects.create(
            activity="belajar")
        self.member = Member.objects.create(name="Budi")

    def test_instance_created(self):
        self.assertEqual(Activity.objects.count(), 1)
        self.assertEqual(Member.objects.count(), 1)

    def test_str(self):
        self.assertEqual(str(self.activity), "belajar")
        self.assertEqual(str(self.member), "Budi")

class FormTest(TestCase):

    def test_form_is_valid(self):
        activity_form = ActivityForm(data={
            "activity": "belajar",
        })
        self.assertTrue(activity_form.is_valid())
        member_form = MemberForm(data={
            'name': "budi"
        })
        self.assertTrue(member_form.is_valid())

    def test_form_invalid(self):
        activity_form = ActivityForm(data={})
        self.assertFalse(activity_form.is_valid())
        member_form = MemberForm(data={})
        self.assertFalse(member_form.is_valid())


class ViewsTest(TestCase):
    def setUp(self):
        self.client = Client()
        self.list_activity = reverse("activity_list")
        self.add_activity = reverse("add_activity")

    def test_GET_list_activity(self):
        response = self.client.get(self.list_activity)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'activity_list.html')

    def test_GET_add_activity(self):
        response = self.client.get(self.add_activity)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'add_activity.html')

    def test_POST_add_activity(self):
        response = self.client.post(self.add_activity,
                                    {
                                        'activity': 'belajar'
                                    }, follow=True)
        self.assertEqual(response.status_code, 200)

    def test_POST_add_activity_invalid(self):
        response = self.client.post(self.add_activity,
                                    {
                                        'activity': '',
                                    }, follow=True)
        self.assertTemplateUsed(response, 'add_activity.html')
        



# class TestMember(TestCase):
#     def setUp(self):
#         activity = Activity(activity="ngoding")
#         activity.save()

#     def test_member_POST(self):
#         response = Client().post('/add-member/1',
#                                  data={'name': 'abdul'})
#         self.assertEqual(response.status_code, 200)

#     def test_member_GET(self):
#         response = self.client.get('/add-member/1')
#         self.assertTemplateUsed(response, 'add_member.html')
#         self.assertEqual(response.status_code, 200)

    # def test_regist_POST_invalid(self):
    #     response = Client().post('/add-member/1',
    #                              data={'name': ''})
    #     self.assertTemplateUsed(response, 'add_member.html')


class TestApp(TestCase):
    def test_apps(self):
        self.assertEqual(ActivityConfig.name, 'activity')
        self.assertEqual(apps.get_app_config('activity').name, 'activity')

