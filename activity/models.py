from django.db import models

# Create your models here.

class Activity(models.Model):
    activity = models.CharField(max_length=50)
    
    def __str__(self):
        return self.activity



class Member(models.Model):
    name = models.CharField(max_length=50)
    activity = models.ForeignKey(Activity, on_delete=models.CASCADE, null=True, blank=True)

    def __str__(self):
        return self.name

    

